class TicTacToeRoom {
  #users;
  #roomId;

  constructor(roomId) {
    this.#roomId = roomId;
    this.#users = [];
  }

  addUser(user) {
    this.#users.push(user);
  }
}

module.exports = TicTacToeRoom;