const DRAW_GAME = 'draw';

class TicTacToeGame {
  #board;
  #humanPlayerMark;
  #machinePlayerMark;

  constructor(humanPlayerMark) {
    this.#humanPlayerMark = humanPlayerMark;
    this.#machinePlayerMark = humanPlayerMark == 'X' ? 'O' : 'X';
    this.#board = Array(9).fill(null);
  }

  humanPlay(boardPosition) {
    this.#board[boardPosition] = this.#humanPlayerMark;

    return { 
      board: this.#board.slice(),
      winner: this.checkWinner()
    };
  }

  IAPlay() {
    let bestPositionInfo = this.minimax(true);
    this.#board[bestPositionInfo.boardIndex] = this.#machinePlayerMark;

    return {
      board: this.#board,
      winner: this.checkWinner()
    };
  }

  minimax(IATurn) {
    let bestPositionInfo = {
      boardIndex: null,
      score: null
    };

    const freePositions = Array();

    this.#board.forEach((mark, index) => {
      if (mark === null) {
        freePositions.push(index);
      }
    });

    let winner = this.checkWinner();

    // Check if there's a winner, don't forget the draw, since can be a bit clumsy IA
    if (winner !== null && winner !== DRAW_GAME) {

      if (winner === this.#machinePlayerMark) {
        // Machine win so the score is +1
        bestPositionInfo = {
          score: 1
        };
      } else {
        // Machine lost then score is -1
        bestPositionInfo = {
          score: -1
        };
      }

    } else {

      // there're not free positions game is finished with a draw
      if (winner == DRAW_GAME) {
        bestPositionInfo = {
          score: 0
        };
      } else {
        let freePositionInfo = {};
        if (IATurn) {
          bestPositionInfo = {
            score: -100000
          };

          freePositions.forEach((index) => {
            this.#board[index] = this.#machinePlayerMark;
            freePositionInfo = this.minimax(!IATurn);

            if (bestPositionInfo.score < freePositionInfo.score) {
              bestPositionInfo = {
                boardIndex: index,
                score: freePositionInfo.score
              };
            }

            this.#board[index] = null;
          });

        } else {
          bestPositionInfo = {
            score: 100000
          };

          freePositions.forEach((index) => {
            this.#board[index] = this.#humanPlayerMark;
            freePositionInfo = this.minimax(!IATurn);

            if (bestPositionInfo.score > freePositionInfo.score) {
              bestPositionInfo = {
                boardIndex: index,
                score: freePositionInfo.score
              };
            }

            this.#board[index] = null;
          });
        }
      }
    }

    return bestPositionInfo;
  }

  checkWinner() {
    let winner = null;

    const WINNER_CONFIGURATIONS = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];

    const config = WINNER_CONFIGURATIONS.find(
      config => (
        this.#board[config[0]] !== null &&
        this.#board[config[0]] == this.#board[config[1]] &&
        this.#board[config[1]] == this.#board[config[2]]
      )
    );

    if (config) {
      winner = this.#board[config[0]];
    } else {
      const isDraw = this.#board.every(mark => mark !== null);
      if (isDraw) {
        winner = DRAW_GAME;
      } 
    }

    return winner;
  }
}

module.exports = TicTacToeGame;