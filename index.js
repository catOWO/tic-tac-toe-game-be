const express = require('express');
const { Server } = require('socket.io');
const http = require('http');
const cors = require('cors');
const TicTacToeGame = require('./tic-tac-toe-game');

const PORT = process.env.PORT || 5000;

const router = require('./router');

const app = express();
const server = http.createServer(app);
const io = new Server(server, {cors: {
  origin: [
    "https://venerable-sunshine-a0709f.netlify.app",
    "http://localhost:3000" // for development
  ],
  methods: ["GET", "POST"]
}});
const gameMap = new Map();

app.use(cors());
app.use(router);

io.on('connection', (socket) => {
  console.log('We have a new game!');

  socket.on('disconnect', () => {
    console.log('Player has left!');
  });

  socket.on('joinGame', ({ roomId }) => {
    console.log(`Player join to room ${roomId}`);
    gameMap.set(roomId, new TicTacToeGame('O'));
    socket.join(roomId);
  });

  socket.on('humanPlayerClick', ({ squareId, roomId }) => {
    console.log(squareId, ' playHuman ', roomId);
    const tictactoeGame = gameMap.get(roomId);
    const result = tictactoeGame.humanPlay(squareId);
    socket.emit('humanPlayed', result);
  });

  socket.on('iaPlaying', ({ roomId }) => {
    const tictactoeGame = gameMap.get(roomId);
    const result = tictactoeGame.IAPlay();
    socket.emit('iaPlayed', result);
  });
});

server.listen(PORT, () => console.log(`Server has started on port ${PORT}`));